﻿using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour, ICollidable
{
    public GameObject SpawnOnPickup; 
    public void CollidedWith(GameObject source)
    {
        if (SpawnOnPickup != null)
        {
            var spawned = com.clydebuiltgames.Pooling.PoolTracker.Get(SpawnOnPickup);
            spawned.transform.position = this.transform.position;
            spawned.transform.rotation = this.transform.rotation;
            spawned.SetActive(true);
        }
        GameStateManager.Instance.PickupItem(this.name);
        this.gameObject.SetActive(false);
    }
    void Start()
    {
    }
    private bool hasCheckedPickup = false;
    private void Update()
    {
        if (GameStateManager.Instance && hasCheckedPickup == false)
        {
            hasCheckedPickup = true; 
            if (GameStateManager.Instance != null && GameStateManager.Instance.GetPickup(this.name))
            {
                this.gameObject.SetActive(false);
            }
        }

    } 
}
