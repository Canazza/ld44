﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyUIController : MonoBehaviour
{
    public List<Image> Keys;
    public List<string> KeyNames;
    private void Start()
    {
        UpdateKeys();
    }

    public void UpdateKeys () {
        for (int i = 0; i < KeyNames.Count; i++) {
            var isPickedUp = GameStateManager.Instance.GetPickup(KeyNames[i]);
            Keys[i].color = isPickedUp ? Color.white : Color.black;
        }
	}
}
