﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float AnglePerSecond;
    private float angle = 0; 

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(Vector3.forward, AnglePerSecond * Time.deltaTime);
    }
}
