﻿using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour, ICollidable {
    public int CoinWorth = 1;
    public int LivesWorth = 0;
    public GameObject SpawnOnPickup;
    public void CollidedWith(GameObject source)
    {
        if (SpawnOnPickup != null)
        {
            var spawned = com.clydebuiltgames.Pooling.PoolTracker.Get(SpawnOnPickup);
            spawned.transform.position = this.transform.position;
            spawned.transform.rotation = this.transform.rotation;
            spawned.SetActive(true);
        }
        if(CoinWorth > 0)
            GameStateManager.Instance.AddCoins(CoinWorth);
        if(LivesWorth > 0)  
            GameStateManager.Instance.AddLife(LivesWorth);
        this.gameObject.SetActive(false);
    }
}
