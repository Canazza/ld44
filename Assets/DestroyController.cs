﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyController : MonoBehaviour {
    public void DisableItem()
    {
        //var animator = this.GetComponent<Animator>();
        //animator.playbackTime = 0;
        this.gameObject.SetActive(false);
    }
}
