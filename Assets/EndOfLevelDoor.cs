﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfLevelDoor : MonoBehaviour {
    public GameObject DoorClosed;
    public GameObject DoorCollider;
    public List<SpriteRenderer> Keys;
    public List<string> KeyNames;
    private void Start()
    {
        UpdateKeys();
    }

    public void UpdateKeys()
    {
        int pickedUp = 0;
        for (int i = 0; i < KeyNames.Count; i++)
        {
            var isPickedUp = GameStateManager.Instance.GetPickup(KeyNames[i]);
            if (isPickedUp) pickedUp++;
            Keys[i].color = isPickedUp ? Color.white : Color.black;
        }
        if(pickedUp >= KeyNames.Count)
        {
            DoorClosed.SetActive(false);
            DoorCollider.SetActive(true);
        } else
        {
            DoorClosed.SetActive(true);
            DoorCollider.SetActive(false);
        }
    }
}
