﻿using com.clydebuiltgames.Controls;
using InControl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.PlayerControl
{
    public class PlatformController : MonoBehaviour
    {
        [Tooltip("How far the player moves in 1 second")]
        public float WalkSpeed = 3;
        [Tooltip("How far the player moves in 1 second")]
        public float RunSpeed = 6;
        [Tooltip("Gravity, in Units/Second")]
        public float Gravity = 10;
        [Tooltip("Maximum Fall Speed in Units/Second")]
        public float FallSpeed = 10;
        [Tooltip("Jump Speed in Units/Second")]
        public float JumpSpeed = 8;
        public float JumpTime = 0.2f;
        public float SuperJumptime = 0.4f;
        public int HorizontalAxisValue { get; set; }
        public int VerticalAxisValue { get; set; }
        public bool JumpPressed { get; set; }
        public bool JumpDown { get; set; }
        public UnityEvent OnTeleport;

        internal void TeleportTo(Vector3 position)
        {
            OnTeleport.Invoke();
            this.transform.position = position;
        }

        //public bool JumpBuffered { get; set; }
        //public float JumpBufferTime = 0.2f;
        public bool IsSliding;
        public bool IsOverLadder;
        public bool IsOnLadder;
        public bool DownPressed { get; set; }
        public bool UpPressed { get; set; }
        public bool RunPressed { get; set; }
        private bool IsDroppingDown = false; 
        public ClydeInputController inputController;

        public Vector2 ColliderSize;
        public Vector2 Velocity; 
        public Vector2 GroundNormal;


        public LayerMask SolidAndPlatforms, SolidOnly,Ladders;
        public bool IsGrounded { get; private set; }
        public float airbourneFor = 0;
        /*private IEnumerator BufferJump()
        {
            for (float t = 0; t < JumpBufferTime; t += Time.deltaTime)
            {
                if (JumpPressed == false)
                { 
                    break;
                }
                JumpBuffered = true;
                yield return null;
            }
            JumpBuffered = false;
        }*/
        
        public float AirbourneBufferTime = 1;
        public float ClimbSpeed = 1;
        public float SlideSpeed;
        public float MinimumSlideAngle = 20;
        public float SlideAngle = 0;
        private void Update()
        {
            TestOverLadder();
            if(!IsJumping && IsGrounded && DownPressed && !IsSliding)
            {
                if(Vector2.Angle(Vector2.up, GroundNormal) > MinimumSlideAngle)
                { 
                    IsSliding = true;
                    SlideAngle = Vector2.SignedAngle(Vector2.up, GroundNormal);
                }
            }
            if(IsSliding && (!IsGrounded || !DownPressed))
            {
                IsSliding = false;
            }
            if (IsGrounded || IsOnLadder)
            {
                airbourneFor = 0;
            } else
            {
                airbourneFor += Time.deltaTime;
            }
            if (IsSliding)
            {
                Velocity.x = -Mathf.Cos(Mathf.Abs(SlideAngle * Mathf.Deg2Rad)) * SlideSpeed * Mathf.Sign(SlideAngle);
                Velocity.y = Mathf.Sin(-Mathf.Abs(SlideAngle * Mathf.Deg2Rad)) * SlideSpeed; 
            } else
            if (IsOnLadder)
            {
                Velocity.x = HorizontalAxisValue * ClimbSpeed;
                Velocity.y = VerticalAxisValue * ClimbSpeed;
            }
            else
            {
                Velocity.x = HorizontalAxisValue * (RunPressed ? RunSpeed : WalkSpeed);
            }
            if(!IsOnLadder && IsOverLadder)
            {
                if(UpPressed || DownPressed)
                {
                    IsOnLadder = true;
                }
            }
            if(IsOnLadder && !IsOverLadder)
            {
                IsOnLadder = false;
            }
            if (JumpPressed && (IsGrounded || airbourneFor <= AirbourneBufferTime))
            { 
                if (DownPressed && !IsSliding)
                {
                    StartCoroutine(DoDropDown());
                }
                else
                {
                    StartCoroutine(DoJump(IsSliding));
                } 
            }
        }
        private void TestOverLadder()
        {
            var ladder = Physics2D.OverlapPoint(this.transform.position, Ladders);
            IsOverLadder = (ladder != null);
            
        }
        public float DownTestOffsets = 0.3f;
        public float DownTestGroundedExtension = 1.1f;
        public float DownTestAirborneExtension = 1.1f;
        private bool TestDown()
        {
            var downResult = Velocity.y <= 0 ? Test(Vector2.down, ColliderSize.y / 2, (IsDroppingDown|| IsOnLadder) ? SolidOnly : SolidAndPlatforms, IsGrounded?DownTestGroundedExtension:DownTestAirborneExtension,
                Vector2.zero,
                Vector2.right * ColliderSize.x * DownTestOffsets,
                Vector2.left * ColliderSize.x * DownTestOffsets) : null;
            if (downResult.HasValue)
            {
                this.transform.position = new Vector2(this.transform.position.x, (lastHit.point.y + ColliderSize.y / 2));
                GroundNormal = downResult.Value.normal;
                if (downResult.Value.collider.tag == MOVINGPLATFORM_TAG)
                {
                    this.transform.SetParent(downResult.Value.transform);

                }
                else
                {
                    this.transform.SetParent(null);
                }
                IsGrounded = true;
                return true;
            }
            else
            {
                this.transform.SetParent(null);
                IsGrounded = false;
                return false;
            }

        }
        public float LowerSideTestOffset = 0.35f;
        private bool TestUp()
        {
            if (Velocity.y >= 0 && Test(Vector2.up, ColliderSize.y / 2, SolidOnly, 1,
                Vector2.zero,
                Vector2.right * ColliderSize.x * 0.1f,
                Vector2.left * ColliderSize.x * 0.1f) != null)
            {
                this.transform.position = new Vector2(this.transform.position.x, (lastHit.point.y - ColliderSize.y / 2));
                //Velocity.y = 0;
                return true;
            }
            return false;

        }
        private bool TestLeft()
        {

            if ((Velocity.x <= 0) && Test(Vector2.left, ColliderSize.x / 2, SolidOnly, 1f,
                Vector2.zero,
                Vector2.up * ColliderSize.y * 0.35f,
                Vector2.down * ColliderSize.y * LowerSideTestOffset) != null)
            {
                this.transform.position = new Vector2((lastHit.point.x + ColliderSize.x / 2), this.transform.position.y);
                //Velocity.x = 0;
                return true;
            }
            return false;

        }
        private bool TestRight()
        {
            if ((Velocity.x >= 0) && Test(Vector2.right, ColliderSize.x / 2, SolidOnly, 1f,
                Vector2.zero,
                Vector2.up * ColliderSize.y * 0.35f,
                Vector2.down * ColliderSize.y * LowerSideTestOffset) != null)
            {
                this.transform.position = new Vector2((lastHit.point.x - ColliderSize.x / 2), this.transform.position.y);
                //Velocity.x = 0;
                return true;
            }
            return false;

        }
        private const string MOVINGPLATFORM_TAG = "MovingPlatform";
        public float Acceleration = 10f;
        private void FixedUpdate()
        {
            if (IsOnLadder || IsSliding)
            {

            } else { 
                if (!IsGrounded)
                {
                    Velocity.y -= Gravity * Time.deltaTime;
                }
                else
                {
                    Velocity.y = 0;
                }
            }
            if (!IsSliding && !IsOnLadder)
            {
                Velocity.y = Mathf.Clamp(Velocity.y, -FallSpeed, FallSpeed * 2);
                Velocity.x = Mathf.Clamp(Velocity.x, -(RunPressed ? RunSpeed : WalkSpeed), (RunPressed ? RunSpeed : WalkSpeed));
                
            } else
            {
            }

            this.transform.Translate(Velocity * Time.deltaTime);
            bool hitUp = TestUp();
            bool hitRight = TestRight();
            bool hitLeft = TestLeft();
            TestRight();
            TestLeft();
            bool hitDown = TestDown();
            if (hitUp || hitDown) Velocity.y = 0;
            if (hitLeft || hitRight)
            {
                Velocity.x = 0;
                IsSliding = false;
            }
        }
        internal void BounceOffEnemy(GameObject gameObject)
        {
            if(IsSliding)
            {
                return;
            }
            if(JumpPressed)
            {
                this.Velocity.y = JumpSpeed;
                StartCoroutine(DoJump(true)); 
            } else
            {
                this.Velocity.y = Math.Abs(this.Velocity.y);

            }
        } 
        public void KnockBack()
        {
            Velocity.x = -Velocity.x;
            Velocity.y = JumpSpeed;
            IsGrounded = false;
            this.transform.SetParent(null);
        }
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(this.transform.position, ColliderSize);
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position + Vector3.down * ColliderSize.y * LowerSideTestOffset + (Vector3.right * ColliderSize.x / 2),
                            this.transform.position + (Vector3.down * ColliderSize.y / 2) + Vector3.right * ColliderSize.x * DownTestOffsets);
            Gizmos.DrawLine(this.transform.position + Vector3.down * ColliderSize.y * LowerSideTestOffset + (Vector3.left * ColliderSize.x / 2),
                            this.transform.position + (Vector3.down * ColliderSize.y / 2) + Vector3.left * ColliderSize.x * DownTestOffsets);
        }
        RaycastHit2D lastHit;
        RaycastHit2D? Test(Vector2 direction, float ColliderSize, LayerMask ColliderMask, float scale = 1, params Vector2[] Offsets)
        {
            foreach (var offset in Offsets)
            {
                var hit = lastHit = Physics2D.Raycast((Vector2)this.transform.position + offset, direction, ColliderSize * scale, ColliderMask);
                if (hit.collider != null)
                {
                    Debug.DrawLine((Vector2)this.transform.position + offset, (hit.point - offset), Color.green, 1);
                    return hit;
                } else
                {
                    Debug.DrawLine((Vector2)this.transform.position + offset, (Vector2)this.transform.position + offset + direction * ColliderSize * scale, Color.red, 1f);
                }
            }
            return null;

        }
        bool IsJumping = false;
        public InputControlType Jump = InputControlType.Action1;
        IEnumerator DoJump(bool SuperJump = false)
        { 
            if (!SuperJump && (!IsGrounded && (airbourneFor > AirbourneBufferTime))) yield break;
            IsJumping = true;
            IsOnLadder = false;
            IsSliding = false;
            inputController.ConsumeBuffer(Jump);
            IsGrounded = false;
            Velocity.y = JumpSpeed;
            for (float t = 0; t < 1; t += Time.deltaTime / (SuperJump?  SuperJumptime: JumpTime))
            {
                if (IsGrounded)
                {
                    IsJumping = false;
                    yield break;
                } 
                if ((!SuperJump && !JumpDown) || Velocity.y <= 0)
                {
                    Velocity.y = 0;
                    IsJumping = false;
                    yield break;
                }
                Velocity.y = JumpSpeed;
                yield return null;
            }
            Velocity.y = JumpSpeed / 2f;
            IsJumping = false;
        }
        IEnumerator DoDropDown()
        {
            inputController.ConsumeBuffer(Jump);
            IsDroppingDown = true;
            IsSliding = false;
            Velocity.y = -JumpSpeed;
            for (float t = 0; t < 1; t += Time.deltaTime)
            {
                yield return null;
                if (IsGrounded)
                {
                    IsDroppingDown = false;
                    yield break;
                }
            }
            IsDroppingDown = false;
        }
    }
}