﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.clydebuiltgames.PlayerControl
{
    public class PlatformAnimator : MonoBehaviour
    {
        public string Horizontal = "Horizontal";
        public string Vertical = "Vertical";
        public string NormalizedSpeed = "Speed";
        public string Grounded = "Grounded";
        public string Running = "Running";
        public string Climbing = "Climbing";
        public string Sliding = "Sliding";

        public Animator Animator;
        public PlatformController Controller;

        // Update is called once per frame
        void Update()
        {
            Animator.SetBool(Running, Controller.RunPressed && Controller.Velocity.x != 0);
            Animator.SetBool(Grounded, Controller.IsGrounded);
            Animator.SetBool(Climbing, Controller.IsOnLadder);
            Animator.SetBool(Sliding, Controller.IsSliding);
            Animator.SetFloat(Horizontal, Controller.Velocity.x);
            Animator.SetFloat(Vertical, Controller.Velocity.y);
            Animator.SetFloat(NormalizedSpeed, Controller.Velocity.normalized.magnitude);
        }
    }
}