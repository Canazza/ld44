﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.PlayerControl
{
    public class TopDownController : MonoBehaviour
    {
        [HideInInspector]
        public Vector2 MoveDirection;
        [Tooltip("How far the player moves in 1 second")]
        public float MoveSpeed = 1;
        [Tooltip("The transform to rotate to face the direction of movement")]
        public Transform RotationRoot;
        [Tooltip("The radius of the hitbox")]
        public float ColliderSize = 0.5f;
        [Tooltip("Layers to test for collision")]
        public LayerMask ColliderMask;
        [Tooltip("How fast to knock the player back when hit")]
        public float KnockbackSpeed = 1;
        [Tooltip("How long to apply the knockback for when hit")]
        public float KnockbackDuration = 0.2f;
        public void SetX(int x)
        {
            MoveDirection.x = Mathf.Clamp(x, -1, 1);
        }
        public void SetY(int y)
        {
            MoveDirection.y = Mathf.Clamp(y, -1, 1);
        } 
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(this.transform.position, new Vector3(ColliderSize * 2, ColliderSize * 2, 0));
        }
        private void FixedUpdate()
        {
            this.transform.Translate(MoveDirection.normalized * MoveSpeed * Time.deltaTime);
            if (MoveDirection.magnitude > 0) RotationRoot.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(MoveDirection.y, MoveDirection.x) * Mathf.Rad2Deg);
            Test(Vector2.left);
            Test(Vector2.right);
            Test(Vector2.up);
            Test(Vector2.down);
        }
        void Test(Vector2 direction)
        {

            var hit = Physics2D.Raycast(this.transform.position, direction, ColliderSize, ColliderMask);
            if (hit.collider != null)
            { 
                this.transform.position = hit.point - direction * ColliderSize; 
            }

        }
        public void KnockBack(Vector2 Source)
        {
            StartCoroutine(DoKnockBack(Source));
        }
        private IEnumerator DoKnockBack(Vector2 Source)
        {
            if (KnockbackDuration <= 0 || KnockbackSpeed <= 0) yield break;
            var direction = ( (Vector2)this.transform.position - Source).normalized; // - MoveDirection.normalized;
            for(float t = 0; t < 1; t += Time.deltaTime / KnockbackDuration)
            {
                yield return null;
                this.transform.Translate(direction * Time.deltaTime * KnockbackSpeed);
            }
        }
    }

}