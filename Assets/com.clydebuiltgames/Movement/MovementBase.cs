﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Movement
{
    public abstract class MovementBase:MonoBehaviour
    {
        protected IEnumerator MoveAtConstantSpeed(Vector3 Start, Vector3 End, float Speed, AnimationCurve Easing = null)
        { 
            yield return MoveToPosition(Start, End, Vector3.Distance(Start, End) / Speed, Easing);
        }
        protected IEnumerator MoveToPosition(Vector3 Start, Vector3 End, float MovementTime, AnimationCurve Easing = null)
        {
            float timeToComplete = MovementTime;
            if (timeToComplete < 0 || timeToComplete == Mathf.Infinity)
            {
                yield break;
            }
            for (float t = 0; t < 1; t += Time.deltaTime / timeToComplete)
            {
                this.transform.localPosition = Vector3.LerpUnclamped(Start, End, Easing != null ? Easing.Evaluate(t) : t);
                yield return null;
            }
            this.transform.localPosition = End;
        }
    }
}
