﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace com.clydebuiltgames.Movement
{
    public class Bullet : MonoBehaviour
    {
        public float Speed = 4;
        public float Lifetime = 4;
        private float LifeLeft = 0;
        private void OnEnable()
        {
            LifeLeft = Lifetime;
        }

        void Update()
        {
            LifeLeft -= Time.deltaTime;
            if (LifeLeft <= 0) this.gameObject.SetActive(false);
        }
        private void FixedUpdate()
        {
            this.transform.Translate(this.transform.right * Speed * Time.deltaTime, Space.World);
        }
    }
}