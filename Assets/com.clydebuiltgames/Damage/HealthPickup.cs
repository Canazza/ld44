﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Damage
{
    public class HealthPickup : MonoBehaviour, ICollidable
    {
        public int HealthToHeal;
        public UnityEvent OnPickup;
        public void CollidedWith(GameObject source)
        {
            var healable = source.GetComponent<IHealable>();
            if (healable != null)
            {
                if (healable.HealDamage(HealthToHeal, this.gameObject, DamageType.Healing))
                {
                    OnPickup.Invoke();
                }
            }
        }
    }
}
