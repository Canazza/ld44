﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Pooling
{
    public class GlobalSpawner:MonoBehaviour
    {
        public static GlobalSpawner Instance; 
        private void Start()
        {
            Instance = this; 
        }
        public void Spawn(GameObject Prefab, Transform Position, bool Reorient = true,  bool Reparent = false)
        {
            GameObject instance = PoolTracker.Get(Prefab);
            if (Reparent) instance.transform.SetParent(Position);
            instance.transform.position = Position.position;
            if (Reorient) instance.transform.rotation = Position.rotation;
            instance.SetActive(true);
        }
    }
}
