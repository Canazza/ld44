﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using UnityEditor;
using UnityEngine;

namespace com.clydebuiltgames.EditorScripts
{
    /// <summary>
    /// A better selector for KeyCodes than the built-in one
    /// This is filled with work-arounds and hacks, like all good Unity Editor Scripts should have
    /// </summary>
    [CustomPropertyDrawer(typeof(KeyCode))]
    public class KeyCodeDrawer:UnityEditor.PropertyDrawer
    {

        public List<KeyCode> codes;
        FilteredSelectionPopup currentPopup;
        string currentId = string.Empty;
        bool updateCurrentValue = false;
        int updatedKeyCode;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            KeyCode selected = (KeyCode)property.intValue;
            codes = GetCodes();
            int index = codes.IndexOf(selected);
             
            if (GUI.Button(position, ((KeyCode)property.intValue).ToString()))
            {
                currentId = property.propertyPath;
                currentPopup = new FilteredSelectionPopup(codes.Cast<object>().ToList(), index, (s) => {
                    updatedKeyCode = (int)s;
                    updateCurrentValue = true;
                });
                PopupWindow.Show(position, currentPopup);
            }
            //Because of the way Unity's Editor GUI works we have to make sure we're on the right element before applying the updated key code
            //This is a horrible hack but I've not figured out a better way to do it.
            //This is (or will) probably causing some weird unintended behaviour somewhere, somewhen, somehow.
            if(updateCurrentValue && currentId == property.propertyPath)
            {
                updateCurrentValue = false;
                currentId = string.Empty;
                property.intValue = updatedKeyCode;
            }
            EditorGUI.EndProperty();
        }

        private List<KeyCode> GetCodes()
        {
            return Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>().ToList();
        }
    }
}
