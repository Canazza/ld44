﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Utils
{
    public static class Persistence
    {
        /// <summary>
        /// Persists data to local storage
        /// </summary>
        /// <typeparam name="T">Type of object to serialise</typeparam>
        /// <param name="Key">Key used to save the data to</param>
        /// <param name="Value">Data to save</param>
        public static void Save<T>(string Key, T Value)
        {
            string JSON = JsonUtility.ToJson(Value);
            PlayerPrefs.SetString(Key, JSON);
            PlayerPrefs.Save();
        }
        /// <summary>
        /// Loads data from local storage
        /// </summary>
        /// <typeparam name="T">Type of object to deserialise</typeparam>
        /// <param name="Key">Key used to load the data from</param> 
        /// <returns></returns>
        public static T Load<T>(string Key)
        {
            if (!PlayerPrefs.HasKey(Key)) return default(T);
            string JSON = PlayerPrefs.GetString(Key);
            return JsonUtility.FromJson<T>(JSON);
        }
        public static void Clear(string Key)
        {
            PlayerPrefs.DeleteKey(Key);
            PlayerPrefs.Save();
        }
    }
}
