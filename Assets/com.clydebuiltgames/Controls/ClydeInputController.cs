﻿using com.clydebuiltgames.Controls;
using InControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Controls
{
    public class ClydeInputController:MonoBehaviour
    {
        
        private List<Guid> DisableControlList = new List<Guid>();
        InputDevice CurrentInputDevice => InputManager.ActiveDevice;
        //Bunch of Unity Events used in the Controls namespace so you can copy just the Controls folder and it should all work
        [System.Serializable]
        public class CIC_Int_Event : UnityEvent<int> { }
        [System.Serializable]
        public class CIC_String_Event : UnityEvent<string> { }
        [System.Serializable]
        public class CIC_Bool_Event : UnityEvent<bool> { }
        public CIC_Int_Event OnHorizontalChanged, OnVerticalChanged;
        public CIC_Bool_Event OnUpPressed, OnDownPressed;
        public float HorizontalAxis, VerticalAxis;
        [System.Serializable]
        public class CIC_ButtonPress
        {
            public InputControlType Button; 
            public float BufferFor = 0; 
            public bool IsBuffering;
            public CIC_Bool_Event IsPressed,IsBuffered;
        } 
        [Tooltip("Single button pressses, ie Jump or Shoot")]
        public List<CIC_ButtonPress> ButtonPresses;
        private void OnEnable()
        {
            DisableControlList = new List<Guid>();
    }
        public void Update()
        {
            if (DisableControlList.Count > 0)
            {
                HorizontalAxis =0;
                VerticalAxis =0;
                OnHorizontalChanged.Invoke(0);
                OnVerticalChanged.Invoke(0);
                OnUpPressed.Invoke(false);
                OnDownPressed.Invoke(false);
                foreach (var bp in ButtonPresses)
                {
                    bp.IsPressed.Invoke(false);
                }
            }
            else
            {
                HorizontalAxis = CurrentInputDevice.DPadX;
                VerticalAxis = CurrentInputDevice.DPadY;
                OnHorizontalChanged.Invoke(Mathf.RoundToInt(CurrentInputDevice.DPadX));//.GetAxis(HorizontalAxis));
                OnVerticalChanged.Invoke(Mathf.RoundToInt(CurrentInputDevice.DPadY));
                OnUpPressed.Invoke(CurrentInputDevice.DPadUp);
                OnDownPressed.Invoke(CurrentInputDevice.DPadDown);
                foreach (var bp in ButtonPresses)
                {
                    if (bp.BufferFor > 0 && !bp.IsBuffering && CurrentInputDevice.GetControl(bp.Button).IsPressed)
                    {
                        StartCoroutine(BufferInput(bp));
                    }
                    bp.IsPressed.Invoke(CurrentInputDevice.GetControl(bp.Button).IsPressed);
                }
                /*OnHorizontalChanged.Invoke(CInput.GetAxis(HorizontalAxis));
                OnVerticalChanged.Invoke(CInput.GetAxis(VerticalAxis));
                OnUpPressed.Invoke(CInput.GetAxis(VerticalAxis) > 0);
                OnDownPressed.Invoke(CInput.GetAxis(VerticalAxis) < 0);
                foreach (var bp in ButtonPresses)
                {
                    if (bp.BufferFor > 0 && !bp.IsBuffering && CInput.GetButtonDown(bp.Button))
                    {
                        StartCoroutine(BufferInput(bp));
                    }
                    bp.IsPressed.Invoke(CInput.GetButton(bp.Button)); 
                }*/
            }
        }
        public void ConsumeBuffer(InputControlType buttonName)
        {
            ButtonPresses.Where(bp=>bp.Button == buttonName).ToList().ForEach(bp => bp.IsBuffering = false);
        }
        private IEnumerator BufferInput(CIC_ButtonPress bp)
        {
            if (bp.IsBuffering) yield break;
            bp.IsBuffering = true;
            if (!CurrentInputDevice.GetControl(bp.Button).IsPressed)
            {
                bp.IsBuffered.Invoke(false);
                bp.IsBuffering = false;
                yield break;
            }
            for(float t= 0; t < bp.BufferFor; t+= Time.deltaTime)
            {
                if(!bp.IsBuffering || (CurrentInputDevice.GetControl(bp.Button).WasReleased))
                {
                    bp.IsBuffered.Invoke(false);
                    bp.IsBuffering = false;
                    yield break;
                }
                bp.IsBuffered.Invoke(true);
                yield return null;
            }
            bp.IsBuffered.Invoke(false);
            bp.IsBuffering = false;
        }
        public void DisableControlsFor(float seconds)
        {
            StartCoroutine(DoDisableControls(seconds));
        }
        private IEnumerator DoDisableControls(float seconds)
        {
            //Each call to disable controls gets its own ID, so we don't get weird overlaps and unintended early restoration of controls.
            Guid id = Guid.NewGuid();
            DisableControlList.Add(id);
            yield return new WaitForSeconds(seconds);
            DisableControlList.Remove(id);
        }
    }
}
