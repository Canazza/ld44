﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.clydebuiltgames.Controls; 
using UnityEngine;
using UnityEngine.UI;
namespace com.clydebuiltgames.Controls
{
    public class KeyBindLine : MonoBehaviour
    {
        public ClydeInputController.CIC_String_Event OnName;
        public List<GameObject> KeyBindButtons;


        internal void SetButton(string Name, List<ButtonMap> MappedTo, Action<ButtonMap> RemapCallback)
        {
            OnName.Invoke(Name);
            KeyBindButtons.ForEach(kbb => kbb.SetActive(false));
            for (int i = 0; i < Mathf.Min(MappedTo.Count, KeyBindButtons.Count); i++)
            {
                SetupRemapButton(MappedTo[i], KeyBindButtons[i], RemapCallback);
            }
        }
        private void SetupRemapButton(ButtonMap map, GameObject KeyBindButton, Action<ButtonMap> RemapCallback)
        {
            KeyBindButton.SetActive(true);
            var button = KeyBindButton.GetComponent<Button>();
            var text = KeyBindButton.GetComponentInChildren<Text>();
            text.text = map.Key.ToString();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() =>
            {
                RemapCallback(map);
            });

        }
    }
}