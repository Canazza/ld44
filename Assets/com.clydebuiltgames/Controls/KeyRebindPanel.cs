﻿using com.clydebuiltgames.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.clydebuiltgames.Controls
{
    public class KeyRebindPanel : MonoBehaviour
    {
        public ClydeInputController.CIC_String_Event OnRebindName;
        public int CurrentPlayer = 0;
        public GameObject KeyBindLine, AxisBindLine;
        public GameObject KeyBindPopupPanel;
        private GameObject GetKeyBindLine()
        {
            var kbl = com.clydebuiltgames.Pooling.PoolTracker.Get(KeyBindLine);
            kbl.transform.SetParent(this.transform, false);
            kbl.transform.SetAsLastSibling();
            kbl.SetActive(true);
            return kbl;
        }
        private GameObject GetAxisBindLine()
        {
            var abl = com.clydebuiltgames.Pooling.PoolTracker.Get(AxisBindLine);
            abl.transform.SetParent(this.transform, false);
            abl.transform.SetAsLastSibling();
            abl.SetActive(true);
            return abl;
        }
        [ContextMenu("Refresh Key binds")]
        public void UpdateKeyBinds()
        {
            var playerMap = CInput.Map.PlayerMaps[CurrentPlayer];
            for (int i = 0; i < this.transform.childCount; i++)
            {
                this.transform.GetChild(i).gameObject.SetActive(false);
            }
            ///Axis Mapping
            foreach (var axisMap in playerMap.AxisMaps.OrderBy(x=>x.Name))
            {
                var abl = GetAxisBindLine();
                abl.GetComponent<AxisRebindLine>().SetButton(axisMap, (map, positive) =>
                {
                    StartCoroutine(WaitForKeypress((keyCode) =>
                    {
                        if (positive) map.Positive = keyCode;
                        else map.Negative = keyCode;
                    }, string.Format("{0} ({1})", map.Name, positive ? "Positive" : "Negative")));
                });

            }
            ///Button Mapping
            var groupedMaps = playerMap.ButtonMaps.GroupBy(g => g.Name);
            foreach (var buttonGroup in groupedMaps)
            {
                var kbl = GetKeyBindLine();
                kbl.GetComponent<KeyBindLine>().SetButton(buttonGroup.Key, buttonGroup.ToList(), (map) =>
                {
                    StartCoroutine(WaitForKeypress((keyCode) => map.Key = keyCode, string.Format("{0}",map.Name)));
                });
            }
        }
        private bool IsWaitingForKeypress = false;
        private IEnumerator WaitForKeypress(Action<KeyCode> callback,string RebindName)
        {
            if (IsWaitingForKeypress) yield break;
            IsWaitingForKeypress = true;
            OnRebindName.Invoke(RebindName);
            KeyBindPopupPanel.SetActive(true);
            while (!LastKeyCode.HasValue)
            {
                yield return null;
                //Check Mouse Inputs seperately                
                for (int i = 0; i <= 6; i++)
                {
                    if (Input.GetMouseButtonDown(i))
                    {
                        ///323 is Mouse0, 324 is Mouse1, etc
                        LastKeyCode = (KeyCode)(323 + i);
                    }
                }
            }
            if (LastKeyCode != KeyCode.Escape)
            {
                callback(LastKeyCode.Value); 
            }
            LastKeyCode = null;
            KeyBindPopupPanel.SetActive(false);
            IsWaitingForKeypress = false;
            UpdateKeyBinds();
        }
        private KeyCode? LastKeyCode;
        void OnGUI()
        {           
            //This lovely hack appears to be the best way to get the last raw KeyCode from Unity
            if (Event.current.isKey && Event.current.type == EventType.KeyDown && Event.current.keyCode != KeyCode.None)
            {
                LastKeyCode = Event.current.keyCode;
            }
        }
        private void OnEnable()
        {
            LastKeyCode = null;
            KeyBindPopupPanel.SetActive(false);
            IsWaitingForKeypress = false;
        }
        public void Start()
        {
            UpdateKeyBinds();
        }

    }
}