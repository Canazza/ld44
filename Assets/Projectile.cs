﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Projectile : MonoBehaviour
{
    public float Speed = 10;
    public float Lifetime = 2;
    private float LifeLeft = 0;
    private void OnEnable()
    {
        LifeLeft = Lifetime;
    }

    // Update is called once per frame
    void Update()
    {
        LifeLeft -= Time.deltaTime;
        if (LifeLeft <= 0)
        {
            this.gameObject.SetActive(false);
            return;
        }
    }
    private void FixedUpdate()
    {
        this.transform.Translate(Vector3.up * Speed * Time.deltaTime, Space.Self);
    }
}
