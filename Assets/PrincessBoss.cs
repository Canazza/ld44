﻿using com.clydebuiltgames.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PrincessBoss : MonoBehaviour
{
    public new Rigidbody2D rigidbody2D;
    public float RunningSpeed = 4;
    public float WalkSpeed = 1;
    public float RunningDuration = 1;
    public GameObject ProjectilePrefab;
    public AudioClip ProjectileFireSound;
    public UnityEvent OnFire;
    public float FireEverySeconds = 5;

    public void StartRunning()
    {
        Debug.Log("Start Running");
        CancelInvoke("StopRunning");
        CancelInvoke("FireProjectiles");
        rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * RunningSpeed,rigidbody2D.velocity.y);
        Invoke("StopRunning", RunningDuration);
    }
    public void StopRunning()
    {
        Debug.Log("Stop Running");
        rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * WalkSpeed, rigidbody2D.velocity.y);
        FireProjectiles();
    }
    void FireProjectiles()
    {
        if (this.gameObject.activeSelf == false) return;
        for (float t = 0; t < 360; t += 20)
        {
            var spawned = com.clydebuiltgames.Pooling.PoolTracker.Get(ProjectilePrefab);
            spawned.transform.position = this.transform.position;
            spawned.transform.rotation = Quaternion.Euler(0, 0, t);
            spawned.SetActive(true);
        }
        SFXPlayer.Instance.PlaySound(ProjectileFireSound);
        OnFire.Invoke();
        CancelInvoke("FireProjectiles");
        Invoke("FireProjectiles", FireEverySeconds);

    }
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = rigidbody2D ?? GetComponent<Rigidbody2D>();
        Invoke("FireProjectiles", FireEverySeconds);
    }

}
