﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundWalker : MonoBehaviour
{
    public Vector2 InitialSpeed;
    public bool RandomlyReverseXOnStart;
    public Rigidbody2D rigidbody2d;
    public Animator animator;
    private void OnEnable()
    {
        if (rigidbody2d != null)
        {
            bool reverse = RandomlyReverseXOnStart && Random.Range(1, 100) <= 50;
            rigidbody2d.velocity = new Vector2(reverse ? -InitialSpeed.x : InitialSpeed.x, InitialSpeed.y);
        }
    }
    private void Update()
    {
        if(animator != null)
        {
            animator.SetFloat("XSpeed", rigidbody2d.velocity.x);
            animator.SetFloat("YSpeed", rigidbody2d.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        for (int i = 0; i < collision.contactCount; i++)
        {
            var contact = collision.GetContact(i); 
            if (contact.normal.x < 0)
            {
                rigidbody2d.velocity = new Vector2(-Mathf.Abs(InitialSpeed.x), rigidbody2d.velocity.y);
            }
            if (contact.normal.x > 0)
            {
                rigidbody2d.velocity = new Vector2(Mathf.Abs(InitialSpeed.x), rigidbody2d.velocity.y);
            }
        }
    }
}
