using System;
using System.Collections;
using UnityEngine;
using InControl;


namespace com.clydebuiltgames.input
{
	// This custom profile is enabled by adding it to the Custom Profiles list
	// on the InControlManager component, or you can attach it yourself like so:
	// InputManager.AttachDevice( new UnityInputDevice( "KeyboardAndMouseProfile" ) );
	// 
	public class KeyboardAndMouseProfile : UnityInputDeviceProfile
	{
		public KeyboardAndMouseProfile()
		{
			Name = "Keyboard/Mouse";
			Meta = "A keyboard and mouse combination profile appropriate for FPS.";

			// This profile only works on desktops.
			SupportedPlatforms = new[]
			{
				"Windows",
				"Mac",
				"Linux"
			};

			Sensitivity = 1.0f;
			LowerDeadZone = 0.0f;
			UpperDeadZone = 1.0f;

			ButtonMappings = new[]
            {
                new InputControlMapping
                {
                    Handle = "Run",
                    Target = InputControlType.Action2,
                    Source = KeyCodeButton(KeyCode.LeftControl,KeyCode.RightControl,KeyCode.LeftShift,KeyCode.RightShift)
                },
                new InputControlMapping
                {
                    Handle = "Jump",
                    Target = InputControlType.Action1,
                    Source = KeyCodeButton(KeyCode.Space)
                }
			};

            AnalogMappings = new[]
            {
                new InputControlMapping
                {
                    Handle = "DPad Left",
                    Target = InputControlType.DPadLeft,
					// KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
					Source = KeyCodeButton(KeyCode.A, KeyCode.LeftArrow)
                },
                new InputControlMapping
                {
                    Handle = "DPad Right",
                    Target = InputControlType.DPadRight,
					// KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
					Source = KeyCodeButton(KeyCode.D, KeyCode.RightArrow)
                },
                new InputControlMapping
                {
                    Handle = "DPad Up",
                    Target = InputControlType.DPadUp,
					// KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
					Source = KeyCodeButton(KeyCode.W, KeyCode.UpArrow)
                },
                new InputControlMapping
                {
                    Handle = "DPad Down",
                    Target = InputControlType.DPadDown,
					// KeyCodeAxis splits the two KeyCodes over an axis. The first is negative, the second positive.
					Source = KeyCodeButton(KeyCode.S, KeyCode.DownArrow)
                },
            };
		}
	}
}

