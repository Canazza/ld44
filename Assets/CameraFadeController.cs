﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFadeController : MonoBehaviour
{
    public Transform PositionLocatorPosition;

    public ICinemachineCamera CurrentCamera;

    private void Start()
    {
        if(CurrentCamera == null)
        {
            CurrentCamera = this.GetComponent<CinemachineBrain>().ActiveVirtualCamera;
        }
        if (previousPositionLocator == null)
        {
            GameObject ppl = new GameObject("PreviousPositionLocator");
            previousPositionLocator = ppl.transform;
        }
    }
    public void FadeCamera(CinemachineBrain brain)
    {
        //CurrentCamera = brain.ActiveVirtualCamera;
        Debug.Log("Fading Camera");
        StartCoroutine(DoFade(brain.ActiveBlend));
    }
    public void CameraActivated(ICinemachineCamera cameraA, ICinemachineCamera cameraB)
    {
        if (CurrentCamera == null)
        {
            CurrentCamera = this.GetComponent<CinemachineBrain>().ActiveVirtualCamera;
        }
        Debug.Log("Fading Camera");
        StartCoroutine(DoFade(this.GetComponent<CinemachineBrain>().ActiveBlend));
    } 
    public void PrepareForCameraTransition()
    {
        Debug.Log("Prepare for Camera Transition");
        StartCoroutine(DoPrep());
    }

    private IEnumerator DoPrep()
    { 
        previousPositionLocator.position = PositionLocatorPosition.position;
        var currentFollow = CurrentCamera.Follow;
        CurrentCamera.Follow = previousPositionLocator;
        yield return null;
        yield return null;
        yield return null;
        while (IsBlending)
        { 
            yield return null;
        }
        CurrentCamera.Follow = currentFollow;
    }
    private bool IsBlending = false;
    public CanvasGroup Group;
    public AnimationCurve FadeCurve;
    private Transform previousPositionLocator;
    IEnumerator DoFade(CinemachineBlend blend)
    {
        if (blend == null)
        {
            Debug.LogErrorFormat("Blend was Null");
            yield break;
        }
        IsBlending = true;
        var nextCurrentCamera = blend.CamB;
        
        Time.timeScale = 0;
        for (float t = 0; t < 1; t += Time.unscaledDeltaTime / blend.Duration)
        {
            Time.timeScale = 0;
            Group.alpha = FadeCurve.Evaluate(t);
            nextCurrentCamera.UpdateCameraState(Vector3.up, 1);
            yield return null;
        }
         
        IsBlending = false;
        yield return null;
        CurrentCamera = nextCurrentCamera;
        Time.timeScale = 1;

    } 
    
}
