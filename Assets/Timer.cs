﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour {
    public float Duration = 1;
    public UnityEvent OnTimeOut;
    private void OnEnable()
    {
        Invoke("DoTimeout", Duration);
    }
    void DoTimeout()
    {
        OnTimeOut.Invoke();
    }
}
