﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour {
    public static SFXPlayer Instance { get; set; }
    public List<AudioSource> Channels;
	// Use this for initialization
	void Start () {
        Instance = this;
	}
    int lastChannel = 0;
	public void PlaySound(AudioClip clip)
    {
        lastChannel++;
        lastChannel %= Channels.Count;
        Channels[lastChannel].Stop();
        Channels[lastChannel].clip = clip;
        Channels[lastChannel].Play();
    }
}
