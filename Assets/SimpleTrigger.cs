﻿using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleTrigger : MonoBehaviour,ICollidable {
    public UnityEvent OnTrigger;

    public void CollidedWith(GameObject source)
    {
        OnTrigger.Invoke();
    }
}
