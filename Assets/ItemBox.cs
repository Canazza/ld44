﻿using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemBox : MonoBehaviour,ICollidable {
    public int CollectableCount = 0;
    public int Coins = 0;
    public GameObject SpawnOnHit;
    public string SpawnItemName = "";
    public UnityEvent OnHit,OnEmpty;
    public Transform SpawnPosition;
    private float cooldown = 0;
    public Vector2 cooldownOffset = new Vector2(0, -0.2f);
    public List<Transform> Graphics;
    private void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            Graphics.ForEach(t=>t.localPosition = cooldownOffset);
        } else
        {
            Graphics.ForEach(t => t.localPosition = Vector2.zero); 
        }
    }
    public void CollidedWith(GameObject source)
    {
        if (CollectableCount < 0) return;
        if (cooldown > 0) return;
        cooldown = 0.25f;
        if (SpawnOnHit != null)
        {
            var spawn = com.clydebuiltgames.Pooling.PoolTracker.Get(SpawnOnHit);
            if (!string.IsNullOrEmpty(SpawnItemName)) spawn.name = SpawnItemName;
            spawn.transform.position = SpawnPosition.position;
            spawn.SetActive(true);
        }
        if (Coins > 0) GameStateManager.Instance.AddCoins(Coins);
        CollectableCount--;
        OnHit.Invoke();
        if (CollectableCount < 0)
        {
            OnEmpty.Invoke(); 
        }
    }
}
