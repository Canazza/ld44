﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ControlersFound : MonoBehaviour
{
    public GameObject ListItemPrefab;
    public List<string> CurrentList;
    
    // Update is called once per frame
    void Start()
    {
        
        UpdateList();
    }
    void UpdateList()
    {
        Debug.Log("Updating List");
        Debug.Log(InputManager.ActiveDevice);
        CurrentList = new List<string> { InputManager.ActiveDevice.Name };
        while (this.transform.childCount > 1)
        {
            DestroyImmediate(this.transform.GetChild(1).gameObject);
        } 
        foreach (var joystickName in CurrentList) {
            var instance = Instantiate(ListItemPrefab, this.transform, false);
            instance.SetActive(true);
            instance.SendMessage("SetText", joystickName);
        }
    }
    private void InputManager_OnDeviceDetached(InputDevice obj)
    { 
        UpdateList();
    }

    private void InputManager_OnDeviceAttached(InputDevice obj)
    { 
        UpdateList();
    }
    private void Update()
    {
        UpdateList();
    }
}
