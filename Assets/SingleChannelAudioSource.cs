﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleChannelAudioSource : MonoBehaviour {
    public AudioClip clip;
    public bool PlayOnAwake = false;
    private void OnEnable()
    {
        if(PlayOnAwake)
        {
            SFXPlayer.Instance.PlaySound(clip);
        }
    } 
    public void Play()
    {
        SFXPlayer.Instance.PlaySound(clip);
    }
}
