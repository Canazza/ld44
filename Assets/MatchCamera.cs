﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class MatchCamera : MonoBehaviour
{
    public Camera TargetCamera;
    public Camera OtherCamera; 
    void Update()
    {
        TargetCamera.rect = OtherCamera.rect;
        TargetCamera.orthographic = OtherCamera.orthographic;
        TargetCamera.orthographicSize = OtherCamera.orthographicSize;
    }
}
