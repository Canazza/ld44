﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnableEventHandler : MonoBehaviour {
    public UnityEvent OnEnabled;
    public Vector2 InitialVelocity;
    public  new Rigidbody2D rigidbody2D;
    private void OnEnable()
    {
        OnEnabled.Invoke();
        if (rigidbody2D != null)
        {
            rigidbody2D.velocity = InitialVelocity;
        }
    }
}
