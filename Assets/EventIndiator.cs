﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventIndiator : MonoBehaviour {
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("OnCollisionEnter2D ", collision.gameObject);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("OnCollisionExit2D" , collision.gameObject);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("OnTriggerEnter2D" , collision);

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("OnTriggerExit2D" , collision);

    }
    public void Trigger()
    {
        Debug.Log("Trigger");
    }
}
