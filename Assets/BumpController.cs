﻿using com.clydebuiltgames.Damage;
using com.clydebuiltgames.PlayerControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BumpController : MonoBehaviour {
    public PlatformController Player;
    public Collider2D Collider;
    public ContactFilter2D Filter;
    private Collider2D[] Results = new Collider2D[10]; 
	
	// Update is called once per frame
	void Update () {
        if (!Player.IsGrounded && Player.Velocity.y > 0)
        {
            int hit = Physics2D.OverlapCollider(Collider, Filter, Results);
            if(hit > 0)
            { 
                for(int i = 0; i< hit;i++)
                {
                    var result = Results[i];
                    var collidable = result.GetComponent<ICollidable>();
                    if(collidable != null)
                    { 
                        collidable.CollidedWith(this.gameObject);
                    }
                } 
            }
        }
	}
}
