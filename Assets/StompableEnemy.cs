﻿using com.clydebuiltgames.Damage;
using com.clydebuiltgames.PlayerControl;
using com.clydebuiltgames.Pooling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets
{
    public class StompableEnemy:PassiveDamageSource
    {
        public int MaxHP = 1;
        private int HP;
        public GameObject SpawnOnKill,SpawnOnKnockedOff;
        public UnityEvent OnDeath,OnHit; 
        private void OnEnable()
        {
            HP = MaxHP;
        }
        private bool isInvulnerable = false;
        public void StopInvulnerable()
        {
            isInvulnerable = false;
        }
        void Kill(bool KnockedOff = false)
        {
            OnDeath.Invoke();
            if (KnockedOff && SpawnOnKnockedOff != null)
            {
                GlobalSpawner.Instance.Spawn(SpawnOnKnockedOff, this.transform, false);
            } else 
            if (SpawnOnKill != null) GlobalSpawner.Instance.Spawn(SpawnOnKill, this.transform, false);
            this.gameObject.SetActive(false);
        }
        public override void CollidedWith(GameObject source)
        {
            if (isInvulnerable)
            {
                Debug.Log($"Stompable Collision while Invulnerable!");
                return;
            }
            var platformController = source.GetComponentInParent<PlatformController>();
            if(platformController != null)
            { 
                if(platformController.Velocity.y < 0)
                {
                    platformController.BounceOffEnemy(this.gameObject);
                    HP--;
                    if (HP <= 0) Kill(platformController.IsSliding);
                    else
                    {
                        isInvulnerable = true;
                        OnHit.Invoke();
                    }
                    return;
                }
            }
            Debug.Log($"Stompable Enemy Deals Damage! {platformController} => {platformController.Velocity.y:0.00}");
            base.CollidedWith(source);
        }
    }
}
