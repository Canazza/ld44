﻿using com.clydebuiltgames.Damage;
using com.clydebuiltgames.PlayerControl;
using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatePropertyTransfer : MonoBehaviour
{
    public PlatformController controller;
    public HealthPool healthController;
    public float BaseSpeed = 6;
    public float BaseJump = 6;
    public float[] SpeedValues = new float[] { 1, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 2f };
    public float[] JumpValues = new float[] { 1, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 2f };

    [ContextMenu("Max Out")]
    public void MaxOut()
    {

        healthController.MaxHealth = 10;
        healthController.InitialHealth = 10;
        healthController.Health = 10;
        controller.JumpSpeed = JumpValues[9] * BaseJump;
        controller.RunSpeed = JumpValues[9] * BaseSpeed;
    }
    // Start is called before the first frame update
    void Start()
    {
        healthController.MaxHealth = 10;
        healthController.InitialHealth = GameStateManager.Instance.Health;
        healthController.Health = GameStateManager.Instance.Health; 
        controller.JumpSpeed = JumpValues[GameStateManager.Instance.Jump] * BaseJump;
        controller.RunSpeed = JumpValues[GameStateManager.Instance.Speed] * BaseSpeed;
    } 
}
