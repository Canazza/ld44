﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class Beanstalk : MonoBehaviour
{
    public SpriteRenderer Stalk;
    public Transform Head;
    public Transform EndPlatform;
    public BoxCollider2D Ladder;
    public float GrowSpeed = 1;
    public float Height = 10;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawCube(this.transform.position, Vector2.one * 0.4f);
        Gizmos.DrawCube(this.transform.position + Vector3.up * Height, Vector2.one * 0.4f);
        Gizmos.DrawLine(this.transform.position, this.transform.position + Vector3.up * Height);
    }
    private void Start()
    {
        if (Application.isPlaying)
        {
            this.gameObject.SetActive(false);
        }
    }
    public float T = 0;
    private void OnEnable()
    {
        T = 0;
        Head.localPosition = Vector2.zero;
        Stalk.size = new Vector2(1, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.isPlaying)
        {
            if (T < Height)
            {
                T += Time.deltaTime * GrowSpeed;
            }
            else
            {
                T = Height;
            }
        }
        EndPlatform.localPosition = 
        Head.localPosition = Vector2.up * T;
        Stalk.size = new Vector2(1, T);
        Ladder.size = new Vector2(Ladder.size.x, T+1);
        Ladder.offset = new Vector2(0, (T+1) / 2);
    }
}