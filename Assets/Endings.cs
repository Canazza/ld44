﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Endings : MonoBehaviour
{
    public GameObject Ending1, Ending2, Ending3, Ending4, Ending5;
    public void Start()
    {
        int Lives = GameStateManager.Instance.Lives;
        Ending1.SetActive(false);
        Ending2.SetActive(false);
        Ending3.SetActive(false);
        Ending4.SetActive(false);
        Ending5.SetActive(false);

        if (Lives < 20)
        {
            Ending1.SetActive(true);
        } else if(Lives <= 40)
        {
            Ending2.SetActive(true);
        } else if(Lives <= 80)
        {
            Ending3.SetActive(true);
        } else if(Lives < 100)
        {
            Ending4.SetActive(true);
        } else
        {
            Ending5.SetActive(true);
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
