﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InControl
{
    // @cond nodoc
    [AutoDiscover]
    public class MayflashMDProfile : UnityInputDeviceProfile
    {
        public MayflashMDProfile()
        {
            Name = "Mayflash Megadrive Controller";
            Meta = "Mayflash MD USB Adapter";

            SupportedPlatforms = new[] {
                "Windows",
                "OS X"
            };

            JoystickNames = new[] {
                "Mayflash MD USB Adapter"
            };

            ButtonMappings = new[] {
                new InputControlMapping {
                    Handle = "A",
                    Target = InputControlType.Action1,
                    Source = Button0
                },
                new InputControlMapping {
                    Handle = "B",
                    Target = InputControlType.Action2,
                    Source = Button1
                },
                new InputControlMapping {
                    Handle = "X",
                    Target = InputControlType.Action3,
                    Source = Button2
                },
                new InputControlMapping {
                    Handle = "Y",
                    Target = InputControlType.Action4,
                    Source = Button3
                },
                new InputControlMapping {
                    Handle = "Left Bumper",
                    Target = InputControlType.LeftBumper,
                    Source = Button4
                },
                new InputControlMapping {
                    Handle = "Right Bumper",
                    Target = InputControlType.RightBumper,
                    Source = Button6
                },
                new InputControlMapping {
                    Handle = "View",
                    Target = InputControlType.View,
                    Source = Button7
                },
                new InputControlMapping {
                    Handle = "Menu",
                    Target = InputControlType.Start,
                    Source = Button9
                }
            };

            AnalogMappings = new[] {
                new InputControlMapping {
                    Handle = "DPad Left",
                    Target = InputControlType.DPadLeft,
                    Source = Analog2,
                    SourceRange = InputControlMapping.Range.Negative,
                    TargetRange = InputControlMapping.Range.Negative,
                    Invert = true
                },
                new InputControlMapping {
                    Handle = "DPad Right",
                    Target = InputControlType.DPadRight,
                    Source = Analog2,
                    SourceRange = InputControlMapping.Range.Positive,
                    TargetRange = InputControlMapping.Range.Positive
                },
                new InputControlMapping {
                    Handle = "DPad Up",
                    Target = InputControlType.DPadUp,
                    Source = Analog3,
                    SourceRange = InputControlMapping.Range.Positive,
                    TargetRange = InputControlMapping.Range.Positive
                },
                new InputControlMapping {
                    Handle = "DPad Down",
                    Target = InputControlType.DPadDown,
                    Source = Analog3,
                    SourceRange = InputControlMapping.Range.Negative,
                    TargetRange = InputControlMapping.Range.Negative,
                    Invert = true
                },
            };
        }
    }
}
