﻿using Cinemachine;
using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActivator : MonoBehaviour, ICollidable {
    
    public Cinemachine.CinemachineVirtualCamera vcam;
    public bool HasTarget = false;
    public void CollidedWith(GameObject source)
    {
        HasTarget = true;
    }
    private bool HadTarget = true;
	// Update is called once per frame
	void Update () {
        if (HasTarget && !HadTarget)
        {
            vcam.gameObject.SetActive(true);
        } else if(!HasTarget && HadTarget)
        {
            vcam.gameObject.SetActive(false);
        }
        HadTarget = HasTarget;
        HasTarget = false;
	}
    public void DoTransition(ICinemachineCamera camA,ICinemachineCamera camB)
    {
    }
}
