﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour {
    public StringEvent OnLevelName;
    // Use this for initialization
    void Start() {
        if (Timer > 0)
        {
            Invoke("LoadLevel", Timer);
        }
	}
    public float Timer = 2;
    public void LoadLevel()
    {
        GameStateManager.Instance.LoadLevel();
    }
}
