﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    public BoolEvent CanBuyHealth, CanBuySpeed, CanBuyJump;
    public FloatEvent Health01, Speed01, Jump01;
    public StringEvent HealthCost, SpeedCost, JumpCost;
    public string SpendFormat = "Spend {0} Lives";
    public void UpdateUI()
    {
        CanBuyHealth.Invoke(GameStateManager.Instance.CanBuyHealth);
        CanBuySpeed.Invoke(GameStateManager.Instance.CanBuySpeed);
        CanBuyJump.Invoke(GameStateManager.Instance.CanBuyJump);
        Health01.Invoke(GameStateManager.Instance.Health / 10f);
        Speed01.Invoke(GameStateManager.Instance.Speed / 10f);
        Jump01.Invoke(GameStateManager.Instance.Jump / 10f);
        HealthCost.Invoke(string.Format(SpendFormat, GameStateManager.Instance.CurrentHealthCost));
        SpeedCost.Invoke(string.Format(SpendFormat, GameStateManager.Instance.CurrentSpeedCost));
        JumpCost.Invoke(string.Format(SpendFormat, GameStateManager.Instance.CurrentJumpCost)); 
    }
    private void Start()
    {
        UpdateUI();
    }
}
