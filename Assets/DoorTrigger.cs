﻿using com.clydebuiltgames.Controls;
using com.clydebuiltgames.Damage;
using com.clydebuiltgames.PlayerControl;
using com.clydebuiltgames.Utils;
using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour, ICollidable
{
    public Transform TeleportTarget;
    public GameObject CurrentTarget;
    public BoolEvent IsActivatable;
    public void CollidedWith(GameObject source)
    {        
        CurrentTarget = source;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        if(TeleportTarget != null)
        Gizmos.DrawLine(this.transform.position, TeleportTarget.position);
    }

    // Update is called once per frame
    void Update () {
        if(CurrentTarget != null)
        {
            IsActivatable.Invoke(true);
            if(InputManager.ActiveDevice.GetControl(InputControlType.DPadDown))
            {
                //Time.timeScale = 0;
                (CurrentTarget.GetComponentInParent<PlatformController>()).TeleportTo(TeleportTarget.position);
            }
        } else
        {
            IsActivatable.Invoke(false);
        }
        CurrentTarget = null;	
	}
}
