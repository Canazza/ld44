﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {
    public static GameStateManager Instance;
    public List<string> LevelNames;
    public List<string> LevelLabels;
    [System.Serializable]
    public class GameState
    {
        public int CurrentLevelId = 0;
        public int Coins = 0;
        public int Lives = 80;
        public int SpeedLevel = 0;
        public int JumpLevel = 0;        
        public int Health = 1;

        public List<PickupState> Pickups = new List<PickupState>();
        [System.Serializable]
        public class PickupState
        {
            public string Name;
            public bool PickedUp;
        }
    }
    private static GameState CurrentState
    {
        get
        {
            return _currentState = (_currentState ?? new GameState());
        }
    }
    public string LevelIntroSceneName = "LevelIntro";
    public string GameOverSceneName = "GameOver";
    public string GameCompleteSceneName = "GameOver";

    public StringEvent OnCoins, OnLives, OnLevelName;
    public UnityEvent OnPickup, OnLevelComplete, OnDeath, OnLivesIncreased;
    private static GameState _currentState = new GameState();
    private void Awake()
    {
        Instance = this;
    } 
    private void Start()
    {
        if (SceneManager.GetActiveScene().name != GameCompleteSceneName && CurrentState.CurrentLevelId >= LevelLabels.Count)
        {
            SceneManager.LoadScene(GameCompleteSceneName);
            return;
        }
        if(CurrentState.CurrentLevelId < LevelLabels.Count)
        {
            OnLevelName.Invoke(LevelLabels[CurrentState.CurrentLevelId]);
        }
        OnCoins.Invoke(string.Format("{0}", CurrentState.Coins));
        OnLives.Invoke(string.Format("{0}", CurrentState.Lives));
    }
    public int[] LifeCosts = new int[] { 1, 2, 4,6,12,15,20,25,30,35 };
    public int CurrentHealthCost => (CurrentState.Health-1) < LifeCosts.Length ? LifeCosts[CurrentState.Health - 1] : 0;
    public int CurrentSpeedCost => (CurrentState.SpeedLevel) < LifeCosts.Length ? LifeCosts[CurrentState.SpeedLevel] : 0;
    public int CurrentJumpCost => (CurrentState.JumpLevel) < LifeCosts.Length ? LifeCosts[CurrentState.JumpLevel] : 0;
    public bool CanBuyHealth => (CurrentState.Health - 1) < LifeCosts.Length ? CurrentState.Lives >= CurrentHealthCost : false;
    public bool CanBuySpeed => (CurrentState.SpeedLevel) < LifeCosts.Length ? CurrentState.Lives >= CurrentSpeedCost : false;
    public bool CanBuyJump => (CurrentState.JumpLevel < LifeCosts.Length) ? CurrentState.Lives >= CurrentJumpCost : false;
    public int Health => CurrentState.Health;
    public int Speed => CurrentState.SpeedLevel;
    public int Jump => CurrentState.JumpLevel;

    public int Lives => CurrentState.Lives;
    public void BuyHealth()
    {
        if (!CanBuyHealth) return;
        CurrentState.Lives -= CurrentHealthCost;
        CurrentState.Health ++;
        OnLives.Invoke(string.Format("{0}", CurrentState.Lives));
    }
    public void BuySpeed()
    {
        if (!CanBuySpeed) return;
        CurrentState.Lives -= CurrentSpeedCost;
        CurrentState.SpeedLevel ++;
        OnLives.Invoke(string.Format("{0}", CurrentState.Lives));
    }
    public void BuyJump()
    {
        if (!CanBuyJump) return;
        CurrentState.Lives -= CurrentJumpCost;
        CurrentState.JumpLevel ++;
        OnLives.Invoke(string.Format("{0}", CurrentState.Lives));
    }
    public void AddLife()
    {
        CurrentState.Lives++;
        OnLivesIncreased.Invoke();
        OnLives.Invoke(string.Format("{0}", CurrentState.Lives));
    }
    public void AddLife(int count)
    {
        CurrentState.Lives+= count;
        OnLivesIncreased.Invoke();
        OnLives.Invoke(string.Format("{0}", CurrentState.Lives));
    }
    public void AddCoins(int count)
    {
        CurrentState.Coins+= count;
        while(CurrentState.Coins >= 100)
        {
            CurrentState.Coins -= 100;
            AddLife();
        }
        OnCoins.Invoke(string.Format("{0}", CurrentState.Coins));
    }
    public bool GetPickup(string Name)
    {
        var existing = CurrentState.Pickups.FirstOrDefault(x => x.Name == Name);
        if (existing == null)
        {
            return false;
        }
        else
        {
            return existing.PickedUp;
        }

    }
    public void PickupItem(string Name)
    {
        var existing = CurrentState.Pickups.FirstOrDefault(x => x.Name == Name);
        if(existing == null)
        {
            CurrentState.Pickups.Add(new GameState.PickupState()
            {
                Name = Name,
                PickedUp = true
            });
        } else
        {
            existing.PickedUp = true;
        }
        OnPickup.Invoke();

    }
    public void LevelComplete()
    {
        OnLevelComplete.Invoke();
        StartCoroutine(DoLevelComplete(0));
    }
    public void LevelCompleteWithDelay()
    {
        OnLevelComplete.Invoke();
        StartCoroutine(DoLevelComplete(1));
    }
    private IEnumerator DoLevelComplete(float initialDelay)
    {
        yield return new WaitForSecondsRealtime(initialDelay);
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(1);
        Time.timeScale = 1;
        NextLevel();
    }
    public void PlayerDead()
    {
        OnDeath.Invoke();
        CurrentState.Lives--;
        StartCoroutine(DoPlayerDead());
    }
    private IEnumerator DoPlayerDead()
    {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(1);
        Time.timeScale = 1;
        ResetLevel();
    }
    public void NextLevel()
    {
        CurrentState.CurrentLevelId++;
        SceneManager.LoadScene(LevelIntroSceneName);
    }
    public void ResetLevel()
    {
        if (CurrentState.Lives < 0)
        {
            SceneManager.LoadScene(GameOverSceneName);
        }
        else
        {
            SceneManager.LoadScene(LevelIntroSceneName);
        }
    }
    public void LoadLevel()
    {
        SceneManager.LoadScene(LevelNames[CurrentState.CurrentLevelId]);
    }
    public void LoadLevel(string Name)
    {
        SceneManager.LoadScene(Name);
    }
    public void NewGame()
    {
        _currentState = null;
    }
    public void RestartGame()
    {
        NewGame();
        SceneManager.LoadScene(0);
    }
}
